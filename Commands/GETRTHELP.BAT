@REM\||(
REM
REM Title: 
REM     GETRTHELP.BAT
REM 
REM Namespace:
REM     RYANTOOLS.BAT\Commands
REM
REM Script parameters:
REM     %1: must take a filepath as an argument.
REM
REM Exit code enumeration:
REM     Exit code 0 indicates success.
REM     Exit code 1 indicates invalid input.
REM     Other exit codes derived from command calls.
REM
REM Environment variables:
REM     None 
REM     
REM Usage examples:
REM     Command line example 1:
REM     C:\> GETRTHELP.BAT
REM     
REM     Command line example 2:
REM     C:\> GETRTHELP.BAT SETBRIGHTNESS.BAT
REM
REM     Command line example 3:
REM     C:\> GETRTHELP.BAT ..\Apps\POWERLESSSHELL.BAT
REM 
REM Dependencies:
REM     RTHelp.md must be present 
REM     Any RYANTOOLS.BAT commands or apps that you want help with
REM 
REM Notes:
REM     None
REM --------------------------------------------------------------
REM 
)

CALL :_mainExecution %*
GOTO :EOF

@REM\||(
REM
REM Subroutine :_mainExecution
REM     Parameter %* forwarded from script parameter %*
REM     Return: Exit code
REM ---------------------------------------------------
REM
)
:_mainExecution
    SETLOCAL
        REM Configure the script environment
        CALL :_configureScript
        REM Validate input args
        CALL :_validateArgs %* && (
            CALL :_handleValidArgs %1
        ) || (
            CALL :_handleInvalidArgs
        )
    ENDLOCAL
EXIT /B %ERRORLEVEL%

@REM\||(
REM
REM Subroutine :_configureScript
REM     No parameters.
REM     Return: Exit code
REM ----------------------------
REM
)
:_configureScript
    REM Exit 0 = success
    SET /A "_SUCCESS=0"
    REM Exit 0 = args detected
    SET /A "_ARGS_DETECTED=0"
    REM Exit 1 = no args
    SET /A "_NO_ARGS=1"
EXIT /B %ERRORLEVEL%

@REM\||(
REM
REM Subroutine :_validateArgs
REM     Parameter %* forwarded from script parameter %* for validation.
REM     Return: Exit code
REM -------------------------------------------------------------------
REM
)
:_validateArgs
    IF [%*]==[] (
    	SET /A "_exitCode=%_NO_ARGS%"
    ) ELSE (
        SET /A "_exitCode=%_ARGS_DETECTED%"
    )
EXIT /B %_exitCode%

@REM\||(
REM
REM Subroutine :_handleValidArgs
REM     Parameter %1 forwarded from script parameter %1.
REM     Return: Exit code
REM ----------------------------------------------------
REM
)
:_handleValidArgs
    REM Read all lines beginning with REM ignoring leading whitespace and 
    REM     output to console.
    TYPE %1 | FINDSTR.EXE /I /R /C:"^rem" /C:"^ *rem" %1 | MORE
EXIT /B %_exitCode%

@REM\||(
REM
REM Subroutine :_handleInvalidArgs
REM     Parameter %1 forwarded from script parameter %1.
REM     Return: Exit code
REM ----------------------------------------------------
REM
)
:_handleInvalidArgs
    TYPE RTHelp.md | MORE
EXIT /B %_exitCode%
