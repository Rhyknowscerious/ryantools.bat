# RYANTOOLS.BAT

## Disclaimer

This document is NOT a software license or agreement of any kind.

This documentation and the software library that it descibes is extremely unstable and should only be used as a general guideline for a work-in-progress.

This document and the software it descibes is provided as-is. There is no warranty of any kind implied or express for the documentation OR the software library. USE ENTIRELY AT YOUR OWN RISK.

## Introduction

RYANTOOLS.BAT is a programming library for CMD.EXE and batch file scripting. The purpose of this library project is to provide an intuitive and easy-to-use way to write code for CMD.EXE.

RYANTOOLS.BAT attempts to acheive the following goals:

1. Ease of use
2. Intuitive interface
3. Standardization
4. Features based on modern general programming concepts

**NOTE:** This code is currently not optimized for performance. Frankly, it's quite slow and as it grows, it will become slower. At the time of writing, I'm not sure how to overcome this obstacle but I am aware of the problem and will work on a fix in the future.

**NOTE:** There are 3 test files to demonstrate how to reference namespaces to initialize your custom code. **Take a look at these before you begin using the library**:

RYANTOOLS.BAT/TESTROOT.BAT
RYANTOOLS.BAT/Commands/TESTCOMMANDS.BAT
RYANTOOLS.BAT/Apps/TESTAPPS.BAT

## Goals in detail

### Ease of Use

There are many reasons why scripting for CMD.EXE can be difficult and/or confusing. There aren't any official standards for CMD.EXE scripting. Sometimes syntax for simple concepts can be nightmarish and completely nonsensical. Many features of modern scripting languages, even simple things like functions, or arrays are not supported in CMD.EXE. This library will provide pseudo or simulated counterparts to features found in modern languages like PowerShell, Python, JavaScript. For example there are pseudo-arryas, pseudo-functions, pseudo-namespaces, etc that aren't native to BAT but can be emulated/simulated. Comment based help is included and inspired by PowerShell and Python. Wrappers are provided for syntactical hacks like printing without tailing newline and other hacky/non-intuitive features that are native to BAT but make no sense syntactically.

### Intuitive Interface

There are some real screwy ways to do things in CMD.EXE and batch scripts. For example, to print text to the console without the tailing newline character the, syntax has two very strange syntactical formats:

    C:/> @ECHO OFF
    <NUL SET /P "=This is " & ECHO one line of text.
    This is one line of text.
    
    C:/> @ECHO OFF
    SET /P "=This is " <NUL & ECHO one line of text.
    This is one line of text.
    
RYANTOOLS.BAT provides a wrapper script that uses a much, much more intuitive syntax:
    
    C:/> @ECHO OFF
    CALL ECHONONEWLINE.BAT "This is " & ECHO one line of text.
    This is one line of text.

There will be more convenience added as library development continues.

### Standardization

There are certain aspects of all Machine/User/Shell environments, batch scripts, and subroutines that are available in all instances of each.

For example. The Windows runtime environment has Machine variables, User (aka Local) variables, Shell session variables, and SETLOCAL scope variables to consider while scripting in CMD.EXE, PowerShell.exe, and other languages. 

These are all present and accessible from any CMD.EXE session or from any batch script. But there is no standardized way to approach these things. There's no official documentation. It's a freeforall which typically ends up resulting in highly disorganized and hard-to-read code.

RYANTOOLS.EXE provides official script writing standards for extending the official library. These script writing standards are only required for extending the RYANTOOLS.EXE library itself. You do not have to follow these rules to write your own scripts/apps.

RYANTOOLS.EXE official script writing standards are as follows:

1. Shell environment variables
    1. RYANTOOLSROOT - Absolute path to RYANTOOLS.EXE library root folder
    2. RTCOMMANDSNAMESPACE - Absolute path to %RYANTOOLSROOT%\Commands
    3. RTAPPSNAMESPACE - Absolute path to %RYANTOOLSROOT%\Apps
2. Script and subroutine comment based help
    1. Script level comment requirements
        1. Script title (script filename with extension)
        2. Enclosing namespace (folder path with RYANTOOLS.BAT as root folder)
        3. Script parameters with descriptions
        4. Exit code pseudo enumeration with descriptions
        5. Shell session/environment variables used with descriptions
        6. Script usage examples with descriptions
        7. Dependencies
        8. Any further notes
    2. Subroutine level comment requirements
        1. Subroutine title
        2. Subroutine description (optional)
        3. Subroutine parameters with descriptions
        4. Return type, value, and/or description (be brief)
3. Script and subroutine content requirements
    1. Mandatory script pure virtual (aka abstract) subroutines
        1. :_mainExecution
        2. :_configureScript
    2. Optional abstract subroutines
        1. Argument handling
            1. :_validateArgs
            2. :_handleValidArgs
            3. :_handleInvalidArgs
        2. User input handling
            4. :_validateUserInput
            5. :_handleValidUserInput
            6. :_handleInvalidUserInput

### Modern Programming Features

Batch scripts and CMD.EXE **DO _NOT_** support the following features found in other languages, _natively_:

* Classes (Found in many languages)
* Object prototypes (Found in JavaScript, Lua...)
* Class/Object instantiation (Found in many languages)
* Inheritance (Found in many langues)
* Enumerations (Found in many languages)
* Constants (Found in many languages)
* Arrays/Collections (Found in many languages)
* Hashtables/Dictionaries (Found in many languages)
* Functions/Methods (Found in many languages)
* Script level variable scope (Found in many languages)
* Function/method level variable scope (Found in many languages)
* Enclosing-scope variable exportation (Found in Python using keywords "global" and "local")
* Return values (Found in many languages)
* Return value captures into variables (Found in many languages)
* Error handling (Found in many languages)
* For-in, For-each, while, do-while, do-until, and other common built-in looping constructs (Found in BASH, JavaScript, PowerShell, and many other languages)
* Compiling sourcecode to executable code

RYANTOOLS.BAT attempts to emulate some but not all of these features with:

* Pseudo-constants (in place of constants and enumerations)
* Pseudo-arrays (in place of arrays/collections)
* Subroutines (in places of functions/methods)
* Emulated script and subroutine level variable scope (in place of script and function/method scope)
* Enclosing-scope variable exportation emulation
* Script and subroutine exit codes (in place of return values)
* FOR-command-hack based return value capturing into variables (in place of simple native return value capturing into variables) 
* Standardized argument and user input validation abstract subroutines and conditional execution blocks (in place of error handling)
* FOR-command and label-based recursion (in place of built-in looping constructs from other languages)
* Source code pseudo-compilation (in place of actual compilation)

Neither RYANTOOLS.BAT nor CMD.EXE support the following features but these may be included in the library in the future:

* Classes
* Class/object instantiation
* Hashtables/Dictionaries

RYANTOOLS.BAT has no intention to support the following features anytime soon (at least not by me):

* Object prototypes
* Inheritance