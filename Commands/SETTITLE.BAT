@REM\||(
REM
REM Title: 
REM     SETTITLE.BAT
REM
REM Namespace:
REM     RYANTOOLS.BAT\Commands
REM
REM Script parameters:
REM     Script parameter %1 takes a string, applies it to the window title, 
REM     and 
REM
REM Exit code enumeration:
REM     Exit code 0 indicates success.
REM     Other exit codes derived from command calls.
REM
REM Environment variables:
REM     None
REM
REM Usage example:
REM     C:\> CALL SETTITLE.BAT "Set Title Here"
REM 
REM Dependencies:
REM     None
REM 
REM Notes:
REM     None
REM ----------------------------------------------------------------------
REM 
)

CALL :_mainExecution %1
GOTO :EOF

@REM\||(
REM
REM Subroutine :_mainExecution
REM     Parameter %1: forwarded from script parameter %1
REM     Return: Exit code
REM ----------------------------------------------------
REM
)
:_mainExecution
    SETLOCAL
        CALL :_configureScript
        CALL :_validateArgs %1 && (
            CALL :_setTitle %1
        )
    ENDLOCAL
EXIT /B %ERRORLEVEL%

@REM\||(
REM
REM Subroutine :_configureScript
REM     Parameters: None
REM     Return: Exit code
REM ----------------------------
REM
)
:_configureScript
    REM Exit code 0 indicates success
    SET /A "_SUCCESS=0"
    REM Exit code 2 indicates no script args provided
    SET /A "_NO_ARGUMENTS=2"
EXIT /B %ERRORLEVEL%

@REM\||(
REM
REM Subroutine :_validateArgs
REM     Parameter %1: Forwarded from script params
REM     Return: Exit code
REM ----------------------------------------------
REM
)
:_validateArgs
    IF [%1]==[] (
        SET /A "_argStatus=%_NO_ARGUMENTS%"
    ) ELSE (
        SET /A "_argStatus=%_SUCCESS%"
    )
EXIT /B %_argStatus%

@REM\||(
REM
REM Subroutine :_setTitle
REM     Parameter %1: forwarded from script parameter %1
REM     Return: Exit code
REM ----------------------------------------------------
REM
)
:_setTitle
    SETLOCAL
        SET "_title=%~1"
        TITLE %_title%
    ENDLOCAL
EXIT /B %_EXIT_CODE%
