@REM\||(
REM
REM Title:
REM     TESTFORADMINRIGHTS.BAT
REM
REM Namespace:
REM     RYANTOOLS.BAT\Commands
REM
REM Script parameters:
REM     None
REM
REM Exit code enumeration:
REM     0: Admin true
REM     1: Admin false
REM
REM Environment variables:
REM     None
REM
REM Usage:
REM     CALL TESTFORADMINRIGHTS.BAT && (
REM         ECHO You have elevated priveleges
REM     ) || (
REM         ECHO You don't have elevated priveleges
REM     )
REM
REM Dependencies:
REM     None
REM
REM Notes:
REM     None
REM -----------------------------------------------
REM
)

CALL :_mainExecution
GOTO :EOF

@REM\||(
REM
REM Subroutine :_mainExecution
REM     No parameters
REM     Return: Exit code
REM --------------------------
REM
)
:_mainExecution
    SETLOCAL
        CALL :_configureScript
        CALL :_testForAdminRights
    ENDLOCAL
EXIT /B %ERRORLEVEL%

@REM\||(
REM
REM Subroutine :_configureScript
REM     No parameters
REM     Return: Exit code
REM ----------------------------
REM
)
:_configureScript
    SET /A "_ADMIN_TRUE=0"
    SET /A "_ADMIN_FALSE=1"
EXIT /B %ERRORLEVEL%

@REM\||(
REM
REM Subroutine :_testForAdminRights
REM     No parameters
REM     Return: Admin status
REM --------------------------------------------------
REM
)
:_testForAdminRights
    NET SESSION > NUL 2>&1 && (
        SET "_adminStatus=%_ADMIN_TRUE%"
    ) || (
        SET "_adminStatus=%_ADMIN_FALSE%
    )
EXIT /B %_adminStatus%