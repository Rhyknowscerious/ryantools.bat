RYANTOOLS.BAT
=============

Batch Script Library and Pseudo-Compiler
----------------------------------------

### Introduction ###

RYANTOOLS.BAT is a command library for CMD.EXE batch scripts. It is also a 
    pseudo-compiler for .BAT files. The term pseudo-compiler in this context 
    means that the .BAT code isn't actually compiled into bytecode or machine 
    code but it is converted from .BAT file format to .EXE.
    
There are four main pseudo-namespaces. These are the root namespace ".", the 
    Commands namespace, the Apps namespace, and PseudoCompiler namespace. A 
    pseudo-namespace is a directory filled with .BAT scripts. These were 
    originally designed to prevent name collisions while calling .BAT scripts 
    from within other .BAT scripts. Unfortunately, the main application that 
    makes pseudo-compilation possible does not preserve folder structure so 
    the namespace idea doesn't work very well in pseudo-compiled .EXE files. 
    But it does help in non-compiled apps.
    
There are two ways that RYANTOOLS.BAT is designed to be used. One way is as a 
    commands library for writing other .BAT scripts. The other way is as a 
    pseudo-compiler. Lets look at how to use it as a commands library first.
    
Inspired by ["How to Convert a (Batch File) BAT to EXE [Step-by-Step]" by Nicholas Xuan Nguyen](https://adamtheautomator.com/bat-to-exe/).
Zip natively with [tar.exe](https://superuser.com/questions/110991/can-you-zip-a-file-from-the-command-prompt-using-only-windows-built-in-capabili) in the Windows Command Processor (CMD.EXE)
    
### How to Use it... ###

#### ...As a Commands Library ####

To use RYANTOOLS.BAT as a commands library, you can do something as simple as 
    create your own .BAT script and refer to the RYANTOOLS.BAT\Commands 
    folder like this:
    
    @ECHO OFF
    REM C:\my\working\directory\TestAsLibrary.bat
    
    CALL "C:\path\to\RYANTOOLS.BAT\Commands\INITIALIZEAPP.BAT" "C:\path\to\RYANTOOLS.BAT"
    CALL %RTCOMMANDSNAMESPACE%\GETRTHELP.BAT "%RTCOMMANDSNAMESPACE%\GETRTHELP.BAT"
    CALL %RTCOMMANDSNAMESPACE%\ENVIRONMENTANALYZER.BAT
    PAUSE
    
I'll explain the code above. Make sure to CALL all these commands/scripts 
    because they have a high chance of not performing as expected if you 
    don't. Starting with line 3, we CALL the INITIALIZEAPP command which will 
    setup the environment and tell your script where the correct pseudo-
    namespaces are for the commands. After initializing the environment, you 
    can refer to commands in the commnds namespace by prepending them with 
    the `%RTCOMMANDSNAMESPACE%` namespace.
    
#### ...As an App Repository ####

To use RYANTOOLS.BAT as an app repository, create your app in RYANTOOLS.BAT\Apps and 
    reference the app with a windows shortcut. Here's a quick example: 
    
    @ECHO OFF
    REM C:\path\to\RYANTOOLS.BAT\Apps\FavoriteFood.bat
    
    CALL ..\Commands\INITIALIZEAPP.BAT "%CD%\.."
    SET "fileName=%~n0"
    TITLE %fileName%
    CALL %RTCOMMANDSNAMESPACE%\SETCONSOLESIZE.BAT 40 6
    CALL %RTCOMMANDSNAMESPACE%\GETMENUSELECTION.BAT "What's your favorite food?" ^
        Pizza ^
        Squid ^
        "something else"
    SET /A "_userInput=%ERRORLEVEL%"
    CALL %RTCOMMANDSNAMESPACE%\ECHONONEWLINE.BAT "You chose: "
    IF        %_userInput% EQU 1 (
        SET "food=Pizza"
    ) ELSE IF %_userInput% EQU 2 (
        SET "food=Squid"
    ) ELSE IF %_userInput% EQU 3 (
        SET "food=something else"
    )
    ECHO %food%
    PAUSE
    
Now you can "install" your app by creating a shortcut to it and putting that 
    shortcut wherever you want. This app will require your users to have the 
    RYANTOOLS.BAT library. If you want to create your app and NOT require 
    your users to have the library, you can pseudo-compile the app (or a 
    script) into an .EXE file. Keep reading to find out how.
    
#### ...As a Pseudo-Compiler ####

To use RYANTOOLS.BAT as a pseudo-compiler (aka .BAT to .EXE converter), there 
    are a few requirements for your package (your .EXE file) work. 
    
1. The code execution begins at `RYANTOOLS.BAT\MAIN.BAT`.
2. MAIN.BAT needs to set a variable, `%RTCOMPILEDAPP%` to TRUE so that you 
    can write scripts/apps which can be either pseudo-compiled or not ONLY 
    ONE TIME insead of having a compile-able version and a non-compileable 
    version. This is because of the flat-folder structure in your .EXE 
    package.
3. When you call other commands from `MAIN.BAT`, references to all packaged 
    files must be called as if they are in the same directory (because after 
    pseudo-compilation all the files are moved to the same temp folder during 
    execution. *Referenced files LOSE THEIR DIRECTORY STRUCTURE*.)
4. You have to setup a .SED configuration for the pseudo-compiler to work 
    correctly. The file `RYANTOOLS.BAT\PseudoCompiler\PACKAGECONFIG.SED` is 
    a pre-setup file that you can analyze to see how to setup the pseudo-
    compiler. There are some other examples in the `ConfigBackups` folder.
    Just remember the pseudo-compiler points to PACKAGECONFIG.SED by default 
    so you have to edit/replace that file for the compiler to run. Besides 
    converting your .BAT script/app to .EXE, this is where you tell the 
    pseudo-compiler which files to import.
5. The RYANTOOLS.BAT\COMPILE.EXE command, of course, pseudo-compiles your 
    .EXE package **AND** it can edit any .SED files. By default it creates an 
    .EXE package named OUTPUT.EXE in the RYANTOOLS.BAT\Exe folder. You can 
    rename the .EXE to whatever you want. You can share the .EXE **without** 
    requiring your users to download the entire RYANTOOLS.BAT codebase as a 
    dependency.
    
**To put it plainly, to convert .BAT to .EXE, do this:**

1. **Create/edit `RYANTOOLS.BAT\PseudoCompiler\PACKAGECONFIG.SED` to 
    reference all the files your script/app uses.**
2. **Edit `RYANTOOLS.BAT\MAIN.BAT` to configure your .EXE package's execution 
    main point of entry.**
3. **Run `COMPILE.BAT` to convert your .BAT to .EXE.**

Here's a quick cheat sheet about the pseudo-compiler works:

1. IEXPRESS.EXE uses a configuration file with the ".SED" extension. You can 
    create this file manually but I highly recommend making one with 
    IEXPRESS.EXE using the following prompts.
    
    + Create new Self Extraction Directive file
    + Extract files and run installation command
    + Package title: Bat Script Pseudocompiler
    + No Prompt
    + Do not display a license
    + Select MAIN.BAT from RYANTOOLS.BAT\ and any other files you need for 
          your application to work.
    + Install Program: CMD /C MAIN.BAT
      Post Install Command: <None>
    + Show Window: Default
    + No message
    + Package name and options: C:\path\to\your\executible.exe
      Hide file... checked
      Store files... checked
    + No restart
    + Save Self Extraction Directive (SED) file: C:\path\to\your\new\config\file.SED

2. In the .SED configuration file, under the [STRINGS] heading, there is a 
    variable "AppLaunched" and its value should be the command line 
    option that kicks it all off. I recommend setting it to this value: 
    
        AppLaunched=CMD /C MAIN.BAT
        
    The working directory is NOT RYANTOOLS.BAT\. The working directory is 
    actually a temp folder. The temp folder's full path may be something 
    similar to this (if the username is "UserName"):
    
        C:\Users\UserNa~1\AppData\Local\Temp\IXP000.TMP
    
    "MAIN.BAT" is copied over from RYANTOOLS.BAT\ 
    to the temp folder.
        
3. In the .SED config file, other files can be included in the temp folder 
    with the following variables (filenames only, no preceding directory 
    yet):
    
    Here's an example for the following list of files:
    
        C:\RYANTOOLS.BAT\MAIN.BAT
        C:\RYANTOOLS.BAT\scripts\SCRIPT.BAT
        C:\RYANTOOLS.BAT\scripts\ANOTHERSCRIPT.BAT
        
    Note that there are 2 total different directories with 3 total 
    different files. Enter files and directories in the .SED config file 
    like this:
        
        FILE0=MAIN.BAT
        FILE1=SCRIPT.BAT
        FILE2=ANOTHERSCRIPT.BAT
        [SourceFiles]
        SourceFiles0=C:\RYANTOOLS.BAT\
        SourceFiles1=C:\RYANTOOLS.BAT\scripts\
        [SourceFiles0]
        %FILE0%=
        [SourceFiles1]
        %FILE1%=
        %FILE2%=
    
    NOTE!!!
    It does NOT preserve folder structure. All the imported files are put 
    in the root of the temp folder, no matter where they're getting 
    imported from - even different folders.
    
    If you use the IEXPRESS.EXE wizard, all you have to do is select the 
    files in the file chooser.
        
4. In the .SED config file, the path to the pseudo-compiled application 
    (your .EXE file) is set to a variable "TargetName" set like this:
    
    TargetName=C:\TEST.COMMANDS.BAT.EXE
    
If you're NOT pseudo-compiling bat to exe:

    Create your app in the RYANTOOLS.BAT\Apps directory.
    
    Don't forget to initialize any required namespaces in your app or it 
        won't work. Use INITIALIZEAPP.BAT with an argument to the path to the 
        RYANTOOLS.BAT library's root directory.
    
    Create a shortcut to your app for users to access it.
    
##### EXAMPLE: #####

Here's a working example:

You don't have to do this first but for the example we're going make MAIN.BAT 
    look like this:

    @ECHO OFF
    REM C:\whatever\RYANTOOLS.BAT\MAIN.BAT
    
    REM This environment variable MUST BE TRUE or App pseudocompilation will not work.
    SET RTCOMPILEDAPP=TRUE
    
    CALL TEST.APPS.BAT
    
As you can see, we're setting the RTCOMPILEDAPP session variable to TRUE. 
    Then we call TEST.APPS.BAT as if it's in the current folder. It's 
    actually in RYANTOOLS.BAT\Apps before pseudo-compilation and in a temp 
    folder during .EXE runitme along with MAIN.BAT and all other imported 
    files. Did I mention that folder structure is not preserved? This is why.
    
Now, lets make RYANTOOLS.BAT\Apps\TEST.APPS.BAT look like this:
    
    @ECHO OFF
    REM C:\whatever\RYANTOOLS.BAT\Apps\TEST.APPS.BAT
    REM The purpose of this file is to test code from the "Commands" namespace of RYANTOOLS.BAT.
    
    IF [%RTCOMPILEDAPP%]==[TRUE] (
        CALL INITIALIZEAPP.BAT "%CD%"
    ) ELSE (
        CALL ..\Commands\INITIALIZEAPP.BAT "%CD%\.."
    )

    ECHO RYANTOOLS.BAT Root directory: %RYANTOOLSROOT%
    ECHO RYANTOOLS.BAT Commands namespace: %RTCOMMANDSNAMESPACE%
    ECHO RYANTOOLS.BAT Apps namespace: %RTAPPSNAMESPACE%

    REM Create a local scope to prevent creating variables in the CMD.EXE session environment. This creates a script level scope of existance for new variables.
    SETLOCAL

        REM SETTITLE.BAT Demo
        CALL %RTCOMMANDSNAMESPACE%\SETTITLE.BAT TESTCOMMANDS.BAT
        
        REM SETCONSOLESIZE.BAT Demo
        SET "_width=100"
        SET "_height=15"
        CALL %RTCOMMANDSNAMESPACE%\SETCONSOLESIZE.BAT %_width% %_height%

        REM ECHONONEWLINE.BAT Demo
        CALL %RTCOMMANDSNAMESPACE%\ECHONONEWLINE.BAT "This is "
        ECHO one line of text. & ECHO.

        REM GETARGCOUNT.BAT Demo
        CALL %RTCOMMANDSNAMESPACE%\GETARGCOUNT.BAT A B C D E F
        ECHO The previous command invocation was provided with %ERRORLEVEL% arguments. & ECHO.

        REM GETBATTERYLEVEL.BAT Demo
        CALL %RTCOMMANDSNAMESPACE%\ECHONONEWLINE "The current battery level is: "
        CALL %RTCOMMANDSNAMESPACE%\GETBATTERYLEVEL.BAT > NUL
        SET "_batteryLevel=%ERRORLEVEL%"
        ECHO %ERRORLEVEL%%% & ECHO.
        
        REM GETBRIGHTNESS.BAT Demo
        CALL %RTCOMMANDSNAMESPACE%\ECHONONEWLINE "The current brightness level is: "
        CALL %RTCOMMANDSNAMESPACE%\GETBRIGHTNESS.BAT > NUL
        SET "_brightnessLevel=%ERRORLEVEL%"
        ECHO %ERRORLEVEL%%% & ECHO.
        
        REM GETMENUSELECTION.BAT Demo
        CALL %RTCOMMANDSNAMESPACE%\GETMENUSELECTION.BAT "Favorite color selection menu." Red Orange Yellow Green Blue Purple Pink Brown Black White "Something else"
        SET /A "_userInput=%ERRORLEVEL%"
        CALL %RTCOMMANDSNAMESPACE%\ECHONONEWLINE.BAT "You chose: "
        IF %_userInput% EQU 1 (
            SET "_color=Red"
        ) ELSE IF %_userInput% EQU 2 (
            SET "_color=Orange"
        ) ELSE IF %_userInput% EQU 3 (
            SET "_color=Yellow"
        ) ELSE IF %_userInput% EQU 4 (
            SET "_color=Green"
        ) ELSE IF %_userInput% EQU 5 (
            SET "_color=Blue"
        ) ELSE IF %_userInput% EQU 6 (
            SET "_color=Purple"
        ) ELSE IF %_userInput% EQU 7 (
            SET "_color=Pink"
        ) ELSE IF %_userInput% EQU 8 (
            SET "_color=Brown"
        ) ELSE IF %_userInput% EQU 9 (
            SET "_color=Black"
        ) ELSE IF %_userInput% EQU 10 (
            SET "_color=White"
        ) ELSE (
            SET "_color=Unknown color"
        )
        ECHO %_color% & ECHO.
        
    ENDLOCAL

    PAUSE
    
Notice, on line 5, the `INITIALIZEAPP.BAT` command is called differently 
    depending on wether the variable `%RTCOMPILEDAPP%` is set or not. 
    Remember, we set this variable in MAIN.BAT. We did it this way because if 
    you execute the TEST.APPS.BAT script directly, your working directory 
    will be the folder that the .BAT script is in. But when you convert it to 
    .EXE, your working directory during execution will be a temp folder.
    
Now that MAIN.BAT points to TEST.APP.BAT and TEST.APP.BAT's working directory is 
    setup correctly (regardless of whether we're executing directly or as a 
    conerted .EXE package) we need to setup PACKAGECONFIG.SED. So the pseudo-
    compiler can put everything together correctly.
    
Next we're going to run RYANTOOLS.BAT\COMPILE.BAT and select option 2 (edit 
    package). We'll follow instructions listed below the horizontal line (scroll down 
    for details.) The new RYANTOOLS.BAT\PACKAGECONFIG.SED file will look like this:

    [Version]
    Class=IEXPRESS
    SEDVersion=3
    [Options]
    PackagePurpose=InstallApp
    ShowInstallProgramWindow=0
    HideExtractAnimation=1
    UseLongFileName=1
    InsideCompressed=0
    CAB_FixedSize=0
    CAB_ResvCodeSigning=0
    RebootMode=N
    InstallPrompt=%InstallPrompt%
    DisplayLicense=%DisplayLicense%
    FinishMessage=%FinishMessage%
    TargetName=%TargetName%
    FriendlyName=%FriendlyName%
    AppLaunched=%AppLaunched%
    PostInstallCmd=%PostInstallCmd%
    AdminQuietInstCmd=%AdminQuietInstCmd%
    UserQuietInstCmd=%UserQuietInstCmd%
    SourceFiles=SourceFiles
    [Strings]
    InstallPrompt=
    DisplayLicense=
    FinishMessage=
    TargetName=C:\path\to\RYANTOOLS.BAT\Exe\OUTPUT.EXE
    FriendlyName=Bat Script Pseudocompiler
    AppLaunched=CMD /C MAIN.BAT
    PostInstallCmd=<None>
    AdminQuietInstCmd=
    UserQuietInstCmd=
    FILE0="MAIN.BAT"
    FILE1="ECHONONEWLINE.BAT"
    FILE2="GETARGCOUNT.BAT"
    FILE3="GETBATTERYLEVEL.BAT"
    FILE4="GETBRIGHTNESS.BAT"
    FILE5="GETMENUSELECTION.BAT"
    FILE6="SETCONSOLESIZE.BAT"
    FILE7="SETTITLE.BAT"
    FILE8="INITIALIZEAPP.BAT"
    FILE9="TEST.APPS.BAT"
    [SourceFiles]
    SourceFiles0=C:\path\to\RYANTOOLS.BAT\
    SourceFiles1=C:\path\to\RYANTOOLS.BAT\Commands\
    SourceFiles2=C:\path\to\RYANTOOLS.BAT\Apps\
    [SourceFiles0]
    %FILE0%=
    [SourceFiles1]
    %FILE1%=
    %FILE2%=
    %FILE3%=
    %FILE4%=
    %FILE5%=
    %FILE6%=
    %FILE7%=
    %FILE8%=
    [SourceFiles2]
    %FILE9%=

If you continue through the prompts, you can create the package via the GUI 
    since you already have IEXPRESS.EXE running. But you can also just run 
    COMPILE.EXE with option 1 if your MAIN.BAT and PACKAGECONFIG.SED are 
    setup. 
    
Now you should see a file in RYANTOOLS.BAT\Exe\OUTPUT.EXE. Now you can rename 
   the .EXE and move it wherever you want because the working directory is 
   always the same temp folder.

---------------------------------------------------------

#### Further rambling and repeating myself: ####

##### If you ARE pseudo-compiling bat to exe: #####
    
You MUST use a package config file for pseudo-compilation. The default 
    configuration is at RYANTOOLS.BAT\PseudoCompiler\PACKAGECONFIG.SED. 
    It is designed to be either edited directly or via the IEXPRESS.EXE 
    wizard. You can save a copy of the config and rename it to create a 
    backup config or you can create multiple configurations for different 
    projects. (Remember to update the corresponding MAIN.BAT file.)
    
Make sure your package config includes all your dependency files or your 
    .EXE won't work. Make sure your .SED file has an entry for 
    "AppLaunched=CMD /C MAIN.BAT". This tells the package what to do upon 
    invoking the .EXE file. Use CMD /K to keep the console open after 
    execution or use PAUSE.EXE. I believe you may even be able to use 
    PowerShell or WSH invocations.

If you reference other packaged scripts/files, they will NOT hold their 
    directory (folder) structure. All calls to packaged files from inside 
    MAIN.BAT must call other packaged files as if they were inside the 
    same directory (folder).

Why? Because IEXPRESS.EXE does not respect directory structure within a 
    package. You'd probably have to include a zip file and extract that 
    to get it to work correctly. That requires a dependency on a language 
    that can actually do that, like JScript, VBScript, PowerShell, or a 
    3rd party language. There are no native zip command line utilities in 
    CMD.EXE.
    
There is a log file at RYANTOOLS.BAT\PseudoCompiler\PseudoCompiler.log. 
    It does not provide the most details but it still can be helpful for 
    troubleshooting if a pseudo-compilation fails.

Execution of your .EXE begins at the top of RYANTOOLS.BAT\MAIN.BAT. So 
    call your commands, scripts, and apps from MAIN.BAT.

