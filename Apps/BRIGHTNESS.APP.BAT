@ECHO OFF
@REM\||(
REM
REM Title:
REM     SETBRIGHTNESS.BAT
REM
REM Namespace:
REM     RYANTOOLS.BAT\Apps
REM
REM Script parameters:
REM     None
REM
REM Exit code enumeration:
REM     Exit code 0 indicates success.
REM     Other exit codes derived from command calls.
REM
REM Environment variables:
REM     RYANTOOLSROOT
REM     RTAPPSNAMESPACE
REM     RTCOMMANDSNAMESPACE
REM
REM Usage:
REM     C:\> CALL BRIGHTNESSAPP.BAT
REM
REM Dependencies:
REM     RYANTOOLS.BAT\Commands\INITIALIZEAPP.BAT
REM     RYANTOOLS.BAT\Commands\SETCONSOLESIZE.BAT
REM     RYANTOOLS.BAT\Commands\GETBATTERYLEVEL.BAT
REM     RYANTOOLS.BAT\Commands\GETBRIGHTNESS.BAT
REM     RYANTOOLS.BAT\Commands\SETBRIGHTNESS.BAT
REM 
REM Notes:
REM     None
REM -----------------------------------------------
REM
)

CALL :_mainExecution
GOTO :EOF

@REM\||(
REM
REM Subroutine :_mainExecution
REM     No parameters
REM     Return: Exit code
REM --------------------------
REM
)
:_mainExecution

    IF [%RTCOMPILEDAPP%]==[TRUE] (
        CALL INITIALIZEAPP.BAT "%CD%"
    ) ELSE (
        CALL ..\Commands\INITIALIZEAPP.BAT "%CD%\.."
    )
    
    REM Configure the environment
    CALL :_configureScript
    
    REM Show current battery level:
    ECHO. & SET /P "=Battery level: " < NUL
    CALL %RTCOMMANDSNAMESPACE%\GETBATTERYLEVEL.BAT

    REM Show current brightness
    SET /P "=Current brightness: " < NUL
    CALL %RTCOMMANDSNAMESPACE%\GETBRIGHTNESS.BAT & ECHO.
    
    SET /P "_input=Enter a new brightness from 0 to 100: "
    CALL %RTCOMMANDSNAMESPACE%\SETBRIGHTNESS.BAT %_input%
    
EXIT /B %ERRORLEVEL%

@REM\||(
REM
REM Subroutine :_configureScript
REM     No parameters
REM     Return: Exit code
REM ----------------------------
REM
)
:_configureScript
    REM Setup console size
    SET /A "_consoleWidth=60"
    SET /A "_consoleHeight=10"
    CALL %RTCOMMANDSNAMESPACE%\SETCONSOLESIZE.BAT %_consoleWidth% %_consoleHeight%
    
    REM Setup the title
    SET "_title=Set Brightness"
    TITLE %_title%
    ECHO %_title%

EXIT /B %ERRORLEVEL%